import java.time.LocalDate;
import java.time.Period;

public class Person {
    public static final int ADULT_AGE = 18;
    private final String firstName;
    private String lastName;
    private float weight;
    private float height;
    private final Gender gender;
    private final LocalDate birthday;

    public Person(final String firstName, String lastName, float weight, float height, final Gender gender, final
    LocalDate birthday) {
        Requires.Str.NotNullOrEmpty(firstName, "firstName");
        Requires.DateTime.NotFuture(birthday, "birthday");
        this.firstName = firstName;
        setLastName(lastName);
        this.weight = weight;
        this.height = height;
        this.gender = gender;
        this.birthday = birthday;
    }

    public String getFullName() {
        return firstName + " " + lastName;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        Requires.Str.NotNullOrEmpty(lastName, "lastName");
        this.lastName = lastName;
    }

    public float getWeight() {
        return weight;
    }

    public void setWeight(float weight) {
        this.weight = weight;
    }

    public float getHeight() {
        return height;
    }

    public void setHeight(float height) {
        this.height = height;
    }

    public Gender getGender() {
        return gender;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public int getAge(){
        return Period.between(birthday, LocalDate.now()).getYears();
    }

    public String toString(){
        return "Full name: " + getFullName() +
                " Birthday: " + getBirthday() +
                " Gender: " + getGender() +
                " Height: " + getHeight() +
                " Weight: " + getWeight();
    }
}
