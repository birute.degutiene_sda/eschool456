import java.time.LocalDate;
import java.util.Arrays;
import java.util.stream.Stream;

public class Demo {
    public static void main(String[] args) {
        Person john = new Person("John", "Smith", 89, 190, Gender.MALE, LocalDate.of(1996, 1, 3));
        Person astrid = new Person("Astrid", "Oat", 57, 159, Gender.FEMALE, LocalDate.of(1999, 10, 10));
        Person steve = new Person("Steve", "Jobs", 68, 172, Gender.MALE, LocalDate.of(2002, 9, 15));

        Person[] persons = {john, astrid, steve};

        System.out.println("18 yo and older:");
        Stream<Person> peopleStream = Arrays.stream(persons);
        peopleStream
                .filter(p -> p.getAge() >= Person.ADULT_AGE)
                .forEach(System.out::println);
    }
}
